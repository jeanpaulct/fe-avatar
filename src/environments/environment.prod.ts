export const environment = {
  production: true,
  API_VERSION: 'v1',
  SERVER_API_URL: 'https://fierce-plateau-88499.herokuapp.com/',
  ITEMS_PAGE: 2,
  URL_PLANET_PROVIDER: 'https://swapi.co/api/planets'
};
