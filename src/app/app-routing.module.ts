import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {navbarRoute} from './layouts';

const LAYOUT_ROUTES: Routes = [navbarRoute];

@NgModule({
  imports: [RouterModule.forRoot([...LAYOUT_ROUTES],
    {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
