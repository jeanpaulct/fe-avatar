import {library} from '@fortawesome/fontawesome-svg-core';
import {faBan, faBars, faHome, faPencilAlt, faPlus, faSave, faThList, faTimes, faUser} from '@fortawesome/free-solid-svg-icons';

//add fa-icons
library.add(faBars);
library.add(faUser);
library.add(faHome);
library.add(faThList);
library.add(faPencilAlt);
library.add(faTimes);
library.add(faSave);
library.add(faBan);
library.add(faPlus);
