import {environment} from '../environments/environment';

// constants
const API_VERSION = environment.API_VERSION;
export const SERVER_API_URL = environment.SERVER_API_URL + API_VERSION;
export const DEFAULT_ITEM_PAGE = !!environment.ITEMS_PAGE ? environment.ITEMS_PAGE : 3;
export const URL_PLANET_PROVIDER = environment.URL_PLANET_PROVIDER;
