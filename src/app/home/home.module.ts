import {NgModule} from '@angular/core';
import {HomeComponent} from './home.component';
import {HOME_ROUTE} from './home.route';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [HomeComponent],
  imports: [RouterModule.forChild([HOME_ROUTE])
  ]
})
export class HomeModule {
}
