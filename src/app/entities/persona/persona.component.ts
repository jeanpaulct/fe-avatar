import {Component, OnInit} from '@angular/core';
import {IPersona} from './persona.model';
import {HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import {PersonaService} from './persona.service';
import {DEFAULT_ITEM_PAGE} from '../../app.constants';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {debounceTime} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.scss']
})
export class PersonaComponent implements OnInit {

  private ngbModalRef: NgbModalRef;
  private _onAlert = new Subject<Alert>();
  private routeData: any;
  personas: IPersona[];
  totalItems: any;
  itemsPerPage: any;
  page: any;
  alerts: Alert[] = [];

  constructor(private modalService: NgbModal, protected personaService: PersonaService
    , protected router: Router, protected activatedRoute: ActivatedRoute) {
    this._onAlert.subscribe((alert) => this.alerts = [alert]);
    this._onAlert.pipe(
      debounceTime(3000)
    ).subscribe(() => this.alerts = []);
    this.itemsPerPage = DEFAULT_ITEM_PAGE;

    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
    });
  }

  ngOnInit() {
    this.loadAll();
  }

  loadAll() {
    this.personaService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: ['id']
      })
      .subscribe(
        (res: HttpResponse<IPersona[]>) => this.paginatePersona(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: any) {
    this.router.navigate(['/persona'], {
      queryParams: {
        page: page,
        size: this.itemsPerPage,
        sort: ['id']
      }
    });
    this.loadAll();
  }

  protected paginatePersona(data: IPersona[], headers: HttpHeaders) {
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.personas = data;
  }

  protected onError(errorMessage: string) {
    this._onAlert.next({type: 'danger', message: errorMessage});
  }

  deleteItem(tmplDelete: HTMLElement, persona: IPersona) {
    this.ngbModalRef = this.modalService.open(tmplDelete, {ariaLabelledBy: 'modal-basic-title'});
    this.ngbModalRef.result.then((result) => {
      this.personaService.delete(persona.id).subscribe(() => this.onDeleteSuccess(persona.id)
        , (res: HttpErrorResponse) => this.onDeleteError(res));
    }, (reason) => {
      this.ngbModalRef = null;
    });
  }

  protected onDeleteSuccess(id: any) {
    this._onAlert.next({type: 'success', message: 'The register was eliminated.'});
    this.personas = this.personas.filter(p => p.id !== id);
    if (this.personas.length === 0) {
      this.loadAll();
    }
  }

  protected onDeleteError(res: HttpErrorResponse) {
    const msgerror = res.error ? res.error.error_message : 'Some problem on delete the person, try again.';
    this._onAlert.next({type: 'danger', message: msgerror});
  }
}

interface Alert {
  type: string;
  message: string;
}
