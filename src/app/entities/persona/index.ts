export * from './persona.component';
export * from './persona.service';
export * from './persona.route';
export * from './persona.model';
export * from './edit/edit.component';
