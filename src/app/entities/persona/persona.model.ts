export interface IPersona {
  id?: any;
  name?: string;
  height?: string;
  mass?: string;
  hair_color?: string;
  gender?: string;
  planet?: string;
}

export class Persona {
  constructor(
    public id?: any,
    public name?: string,
    public height?: string,
    public mass?: string,
    public hair_color?: string,
    public gender?: string,
    public planet?: string,
  ) {
    this.id = id ? id : null;
    this.name = name ? name : null;
    this.height = height ? height : null;
    this.mass = mass ? mass : null;
    this.hair_color = hair_color ? hair_color : null;
    this.gender = gender ? gender : null;
    this.planet = planet ? planet : null;
  }
}
