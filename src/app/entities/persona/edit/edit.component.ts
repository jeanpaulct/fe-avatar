import {Component, OnInit} from '@angular/core';
import {PersonaService} from '../persona.service';
import {ActivatedRoute} from '@angular/router';
import {IPersona, Persona} from '../persona.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, switchMap, tap} from 'rxjs/operators';
import {PlanetService} from './planet.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  private _success = new Subject<string>();

  private persona: IPersona;
  frmUpdate: FormGroup;
  isSaving: boolean;
  errorMenssage: string;

  searching = false;
  searchFailed = false;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term =>
        this._planetService.search(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          }))
      ),
      tap(() => this.searching = false)
    );

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, protected service: PersonaService
    , protected _planetService: PlanetService) {
  }

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({persona}) => {
      this.persona = persona || new Persona();
    });
    this.frmUpdate = this.formBuilder.group({
      id: [this.persona.id, []],
      name: [this.persona.name, []],
      height: [this.persona.height, []],
      mass: [this.persona.mass, []],
      hair_color: [this.persona.hair_color, []],
      gender: [this.persona.gender, []],
      planet: [this.persona.planet, []]
    });

    this._success.subscribe((message) => this.errorMenssage = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.errorMenssage = null);
  }

  previousState() {
    window.history.back();
  }

  save() {
    if (this.frmUpdate.invalid) {
      return;
    }
    this.isSaving = true;
    const dto: IPersona = Object.assign({}, this.frmUpdate.value);
    if (!!dto.id) {
      this.subscribeToSaveResponse(this.service.update(dto));
    } else {
      this.subscribeToSaveResponse(this.service.create(dto));
    }
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(res: HttpErrorResponse) {
    const msgerror = res.error ? res.error.error_message : 'Some problem on perform action';
    this._success.next(msgerror);
    this.isSaving = false;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPersona>>) {
    result.subscribe((res: HttpResponse<IPersona>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError(res));
  }
}
