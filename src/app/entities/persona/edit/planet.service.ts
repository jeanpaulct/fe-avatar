import {Injectable} from '@angular/core';
import {URL_PLANET_PROVIDER} from '../../../app.constants';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

interface IResPlanet {
  count?: any;
  results?: IPlanet[];
}

interface IPlanet {
  name?: String;
}

@Injectable({
  providedIn: 'root'
})
export class PlanetService {
  public resourceUrl = URL_PLANET_PROVIDER;

  constructor(protected http: HttpClient) {
  }

  search(param: string): Observable<String[]> {
    return this.http.get<IResPlanet>(`${this.resourceUrl}/?search=${param}`)
      .pipe(
        map(rs => rs.results.map(p => p.name)),
      );
  }
}
