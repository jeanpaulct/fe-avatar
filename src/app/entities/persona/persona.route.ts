import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {PersonaComponent} from './persona.component';
import {Injectable} from '@angular/core';
import {IPersona, Persona} from './persona.model';
import {PersonaService} from './persona.service';
import {Observable, of} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {HttpResponse} from '@angular/common/http';
import {EditComponent} from './edit/edit.component';

@Injectable({providedIn: 'root'})
export class ResolvePagingParams implements Resolve<any> {

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
    return {
      page: parseInt(page, 10)
    };
  }
}

@Injectable({providedIn: 'root'})
export class PersonaResolve implements Resolve<IPersona> {
  constructor(private service: PersonaService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPersona> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<IPersona>) => response.ok),
        map((catalogo: HttpResponse<IPersona>) => catalogo.body)
      );
    }
    return of(new Persona());
  }
}

export const personaRoute: Routes = [
  {
    path: '',
    component: PersonaComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    }
  },
  {
    path: ':id/edit',
    component: EditComponent,
    resolve: {
      persona: PersonaResolve
    }
  },
  {
    path: 'new',
    component: EditComponent,
    resolve: {
      persona: PersonaResolve
    }
  }
];
