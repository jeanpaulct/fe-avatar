import '../../vendor.ts';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PersonaComponent, EditComponent, personaRoute} from './';
import {RouterModule} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ReactiveFormsModule} from '@angular/forms';

const ENTITY_STATE = [...personaRoute];

@NgModule({
  declarations: [
    PersonaComponent,
    EditComponent
  ],
  imports: [
    ReactiveFormsModule, FontAwesomeModule, NgbModule, CommonModule, RouterModule.forChild(ENTITY_STATE),
  ]
})
export class PersonaModule {
}
