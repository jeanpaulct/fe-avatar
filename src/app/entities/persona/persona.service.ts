import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IPersona} from './persona.model';

import {SERVER_API_URL} from '../../app.constants';

type EntityResponseType = HttpResponse<IPersona>;
type EntityArrayResponseType = HttpResponse<IPersona[]>;

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  public resourceUrl = SERVER_API_URL + '/persona';

  constructor(protected http: HttpClient) {
  }

  create(persona: IPersona): Observable<EntityResponseType> {
    persona.id = null;
    return this.http.post<IPersona>(this.resourceUrl, persona, {observe: 'response'});
  }

  update(persona: IPersona): Observable<EntityResponseType> {
    const id = persona.id;
    persona.id = null;
    return this.http.put<IPersona>(`${this.resourceUrl}/${id}`, persona, {observe: 'response'});
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPersona>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = this.createRequestOption(req);
    return this.http.get<IPersona[]>(this.resourceUrl, {params: options, observe: 'response'});
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

  private createRequestOption = (req?: any): HttpParams => {
    let options: HttpParams = new HttpParams();
    if (req) {
      Object.keys(req).forEach(key => {
        if (key !== 'sort') {
          options = options.set(key, req[key]);
        }
      });
      if (req.sort) {
        req.sort.forEach((val: string) => {
          options = options.append('sort', val);
        });
      }
    }
    return options;
  };

}
