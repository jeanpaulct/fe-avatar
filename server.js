//Install express server
const express = require('express');
const path = require('path');

const app = express();

app.use(express.static(__dirname + '/dist/fe-avatar'));

app.get('/*', function(req,res) {

  res.sendFile(path.join(__dirname+'/dist/fe-avatar/index.html'));
});

app.listen(process.env.PORT || 9090);
